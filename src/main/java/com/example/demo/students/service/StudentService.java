package com.example.demo.students.service;

import com.example.demo.students.entity.StudentEntity;
import java.util.List;

public interface StudentService {
  public List<StudentEntity> getAllStudentEntity();
  public StudentEntity save(StudentEntity studentEntity);
  public StudentEntity update(StudentEntity studentEntity);
  public StudentEntity getById(Long id);

}
