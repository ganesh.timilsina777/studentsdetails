package com.example.demo.students.service;

import com.example.demo.students.entity.StudentEntity;
import com.example.demo.students.repository.StudentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class StudentsServiceImpl implements StudentService {

  @Autowired
  StudentRepository studentRepository;

  @Override
  public List<StudentEntity> getAllStudentEntity() {
    return studentRepository.findAll();
  }

  @Override
  public StudentEntity save(StudentEntity studentEntity) {
    return studentRepository.save(studentEntity);
  }

  @Override
  public StudentEntity update(StudentEntity studentEntity) {
    StudentEntity oldStd = studentRepository.getOne(studentEntity.getId());
    oldStd.setName(studentEntity.getName());
    oldStd.setAddress(studentEntity.getAddress());

    return studentRepository.save(oldStd);
  }

  @Override
  public StudentEntity getById(Long id) {
    return null;
  }
}
