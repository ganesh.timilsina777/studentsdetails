package com.example.demo.students.entity;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "student_details")

public @Data
class StudentEntity implements Serializable {

  @Id
  @Autowired
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "sId")
  private long id;

  public long getId() {
    return id;
  }

  @Column(name = "name")
  private String name;

  @Column(name = "address")
  private String address;

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }
}

