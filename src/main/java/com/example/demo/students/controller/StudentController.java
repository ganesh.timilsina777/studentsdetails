package com.example.demo.students.controller;

import com.example.demo.students.entity.StudentEntity;
import com.example.demo.students.service.StudentService;
import java.rmi.MarshalledObject;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Log4j2
public class StudentController {
  @Autowired
  private StudentService studentService;

  @GetMapping(value = "form/students/new")
  public ModelAndView addNewStudent(){
    ModelAndView mv = new ModelAndView("view/student/new");
    mv.addObject("studentEntity",new StudentEntity());

    Map<String, String> headerInfo = new HashMap<>();
    headerInfo.put("Name","Enter Your Name");
    headerInfo.put("Address","Enter Your Address");
    mv.addObject("headerInfo",headerInfo);


    return mv;

  }

}
